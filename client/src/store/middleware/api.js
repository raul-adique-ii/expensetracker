import axios from 'axios';
import * as actions from '../api';

const api = ({ dispatch }) => (next) => async (action) => {
  if (action.type !== actions.apiCallBegan.type) return next(action);

  const { url, method, data, onDelete, onStart, onSuccess, onError } = action.payload;

  if (onStart) dispatch({ type: onStart });

  next(action);

  try {
    const res = await axios.request({
      baseURL: 'http://localhost:5000',
      url,
      method,
      data,
    });

    // General
    dispatch(actions.apiCallSuccess(res.data.data));
    // Specific
    if (onSuccess) dispatch({ type: onSuccess, payload: res.data.data });
    next(action)
    if (onDelete) dispatch({ type: onDelete, payload: data })
  } catch (error) {
    // General error
    dispatch(actions.apiCallFailed(error.message));
    // Specific
    if (onError) dispatch({ type: onError, payload: error.message });
  }

};

export default api;